# Ekwa Test

As part of an Ekwater technical test, this project allows the retrieval of information from a promo code, and, if 
necessary, provides a file with compatible offers.

## Components and Versions
This project was created on february 2022, and now contains the following components:
* Symfony 6.0.4, installed with skeleton dependencies
* other components :
  * php-cs-fixer
  * phpunit
  * symfony/maker-bundle
  * symfony/http-client

## Install the project in development environment

To begin using this project, clone the repo and install back dependencies:

```bash
git clone git@gitlab.com:fabianrupin/ekwa-test.git
cd ekwa-test
composer install
```

## Documentation

This project only contains a command 
* to check if a promo code is valid, 
* to retrieve the promo code information, 
* to retrieve the offers associated to this promo code, and to generate a json file with the retrieved information 
(code and compatible offers).

### Use

On the command line, simply type the following command:
```bash
bin/console promo-code:validate PROMO_CODE_TO_TEST
```

if the code is valid and there are compatible offers, a json file will be created in the public/compatible_offers folder.

### Test

To run tests, run the following command:
```bash
./bin/phpunit
```

## Authors

- [@fabianrupin](https://www.gitlab.com/fabianrupin)

