<?php

namespace App\Command;

use App\Service\PromoCodeService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'promo-code:validate',
    description: 'A command to check if a promo code is valid and retrieve the associated offers',
)]
class PromoCodeValidateCommand extends Command
{
    public const COMPATIBLE_OFFERS_FILE_PATH = 'public/compatible_offers/';

    public function __construct(private PromoCodeService $promoCodeService)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp('Please give an promo code in argument.
            If it is valid and if offers are associated, a json file will be generated with these offers.')
            ->addArgument('promoCode', InputArgument::REQUIRED, 'The promo code to check');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $promoCodeArgument = $input->getArgument('promoCode');

        $promoCode = $this->promoCodeService->getPromoCodeInfo($promoCodeArgument);

        if (!$promoCode) {
            $io->error('This code is invalid');

            return Command::SUCCESS;
        }

        if ($promoCode->isExpired()) {
            $io->error('This code is expired');

            return Command::SUCCESS;
        }

        $compatibleOffers = $this->promoCodeService->getOffers($promoCodeArgument);

        if (empty($compatibleOffers)) {
            $io->error('This code is valid but no offers was found');

            return Command::SUCCESS;
        }

        $jsonOffersList = $this->promoCodeService->createJsonValidOfferList($compatibleOffers, $promoCode);

        $now = new \DateTime();
        $stringDate = $now->format('Y_m_d');

        $filename = $stringDate.'_'.$promoCode->getCode().'.json';

        if (!file_exists(self::COMPATIBLE_OFFERS_FILE_PATH)) {
            mkdir(self::COMPATIBLE_OFFERS_FILE_PATH);
        }

        file_put_contents(self::COMPATIBLE_OFFERS_FILE_PATH.$filename, $jsonOffersList);

        $io->success('This code is valid, and offers have been found. Please find the generate file in '.self::COMPATIBLE_OFFERS_FILE_PATH.' folder');

        return Command::SUCCESS;
    }
}
