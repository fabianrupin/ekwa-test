<?php

namespace App\Service;

use App\Model\Offer;
use App\Model\PromoCode;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PromoCodeService
{
    public const PROMO_CODE_LIST_URL = 'https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList';
    public const OFFER_LIST_URL = 'https://601025826c21e10017050013.mockapi.io/ekwatest/offerList';

    public function __construct(private HttpClientInterface $httpClient)
    {
    }

    public function getPromoCodeInfo(string $promoCode): ?PromoCode
    {
        $response = $this->httpClient->request(
            'GET',
            self::PROMO_CODE_LIST_URL,
        );

        $codeList = json_decode($response->getContent());

        foreach ($codeList as $code) {
            if ($code->code === $promoCode) {
                return PromoCode::createCode($code);
            }
        }

        return null;
    }

    /** @return array<Offer> */
    public function getOffers(string $promoCode): array
    {
        $response = $this->httpClient->request(
            'GET',
            self::OFFER_LIST_URL,
        );

        $offerList = json_decode($response->getContent());

        $validOffers = [];

        foreach ($offerList as $offer) {
            foreach ($offer->validPromoCodeList as $validPromoCode) {
                if ($promoCode === $validPromoCode) {
                    $validOffers[] = Offer::createOffer($offer);
                }
            }
        }

        return $validOffers;
    }

    /** @param array<Offer> $compatibleOffers */
    public function createJsonValidOfferList(array $compatibleOffers, PromoCode $promoCode): string
    {
        $offerList = [
            'promoCode' => $promoCode->getCode(),
            'endDate' => $promoCode->getEndDate()->format('Y-m-d'),
            'discountValue' => $promoCode->getDiscountValue(),
        ];

        /** @var Offer $offer */
        foreach ($compatibleOffers as $offer) {
            $offerList['compatibleOfferList'][] = [
                'name' => $offer->getName(),
                'type' => $offer->getType(),
            ];
        }

        return json_encode($offerList);
    }
}
