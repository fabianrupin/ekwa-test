<?php

namespace App\Model;

class Offer
{
    private string $type = 'NO_TYPE';

    private string $name = 'NO_NAME';

    private string $description = 'NO_DESCRIPTION';

    /** @var array<string> */
    private array $validPromoCodeList = [];

    public static function createOffer(\stdClass $foundOffer): self
    {
        $offer = new self();

        if (isset($foundOffer->offerType)) {
            $offer->type = $foundOffer->offerType;
        }

        if (isset($foundOffer->offerName)) {
            $offer->name = $foundOffer->offerName;
        }

        if (isset($foundOffer->offerDescription)) {
            $offer->description = $foundOffer->offerDescription;
        }

        if (isset($foundOffer->validPromoCodeList)) {
            foreach ($foundOffer->validPromoCodeList as $validPromoCode) {
                $offer->validPromoCodeList[] = $validPromoCode;
            }
        }

        return $offer;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /** @return array<string> */
    public function getValidPromoCodeList(): array
    {
        return $this->validPromoCodeList;
    }
}
