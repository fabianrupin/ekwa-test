<?php

namespace App\Model;

class PromoCode
{
    private ?string $code = 'NO_CODE';

    private float $discountValue = 0.0;

    private \DateTime $endDate;

    private bool $isExpired = true;

    public function __construct()
    {
        $this->endDate = new \DateTime('01/01/2000');
    }

    public static function createCode(\stdClass $foundCode): self
    {
        $promoCode = new self();

        $promoCode->code = $foundCode->code;

        if (isset($foundCode->discountValue)) {
            $promoCode->discountValue = $foundCode->discountValue;
        }

        if (!isset($foundCode->endDate)) {
            return $promoCode;
        }

        $promoCode->endDate = \DateTime::createFromFormat('Y-m-d', $foundCode->endDate);

        if ($promoCode->endDate > new \DateTime()) {
            $promoCode->isExpired = false;
        }

        return $promoCode;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getDiscountValue(): float
    {
        return $this->discountValue;
    }

    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    public function isExpired(): bool
    {
        return $this->isExpired;
    }
}
