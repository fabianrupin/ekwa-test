<?php

namespace App\Tests;

use App\Model\Offer;
use App\Model\PromoCode;
use App\Service\PromoCodeService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;

class CreateJsonValidOfferListTest extends TestCase
{
    public function testJsonWithOffers(): void
    {
        $expectedJsonString = '{"promoCode":"VALID_CODE","endDate":"2022-05-01","discountValue":2.5,"compatibleOfferList":[{"name":"LIQUID_METHANE_2001","type":"GNL"},{"name":"LIQUID_METHANE_2002","type":"GNL"},{"name":"LIQUID_METHANE_2003","type":"GNL"}]}';

        $foundCode = new \stdClass();
        $foundCode->code = 'VALID_CODE';
        $foundCode->discountValue = 2.5;
        $foundCode->endDate = '2022-05-01';

        $promoCode = PromoCode::createCode($foundCode);

        $offerCompatibleList = [];
        for ($i = 1; $i <= 3; ++$i) {
            $foundOffer = new \stdClass();
            $foundOffer->offerType = 'GNL';
            $foundOffer->offerName = 'LIQUID_METHANE_200'.$i;
            $foundOffer->offerDescription = "Vous n'aurez plus jamais chaud";
            $foundOffer->validPromoCodeList = [
                'VALID_OFFER',
                'OFFER_$i',
            ];
            $offerCompatibleList[] = Offer::createOffer($foundOffer);
        }

        $client = new MockHttpClient();

        $promoCodeService = new PromoCodeService($client);

        $jsonContent = $promoCodeService->createJsonValidOfferList($offerCompatibleList, $promoCode);

        $this->assertJson($jsonContent);
        $this->assertJsonStringEqualsJsonString($expectedJsonString, $jsonContent);
    }
}
