<?php

namespace App\Tests;

use App\Service\PromoCodeService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class GetPromoCodeInfoTest extends TestCase
{
    public const BODY = '[{"code":"EXPIRED","discountValue":2,"endDate":"2019-10-04"},{"code":"VALID","discountValue":1.5,"endDate":"2022-06-20"}]';

    public function testWrongCode(): void
    {
        $promoCodeListMockResponse = new MockResponse(
            self::BODY);

        $client = new MockHttpClient($promoCodeListMockResponse);

        $promoCodeService = new PromoCodeService($client);

        $codeInfo = $promoCodeService->getPromoCodeInfo('WRONG');

        $this->assertNull($codeInfo);
    }

    public function testExpiredCode(): void
    {
        $promoCodeListMockResponse = new MockResponse(
            self::BODY);

        $client = new MockHttpClient($promoCodeListMockResponse);

        $promoCodeService = new PromoCodeService($client);

        $codeInfo = $promoCodeService->getPromoCodeInfo('EXPIRED');

        $this->assertTrue($codeInfo->isExpired());
    }

    public function testValidCode(): void
    {
        $promoCodeListMockResponse = new MockResponse(
            self::BODY);

        $client = new MockHttpClient($promoCodeListMockResponse);

        $promoCodeService = new PromoCodeService($client);

        $codeInfo = $promoCodeService->getPromoCodeInfo('VALID');

        $this->assertSame($codeInfo->getCode(), 'VALID');
    }
}
