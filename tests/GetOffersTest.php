<?php

namespace App\Tests;

use App\Service\PromoCodeService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class GetOffersTest extends TestCase
{
    public const BODY = '[{"offerType":"GAS","offerName":"GAS_OFFRE_1","offerDescription":"desc","validPromoCodeList":["CODE_GAS","CODE_ALL"]},{"offerType":"ELEC","offerName":"ELEC_OFFRE_1","offerDescription":"desc","validPromoCodeList":["CODE_ELEC","CODE_ALL"]}]';

    public function testNoOfferCode(): void
    {
        $promoCodeListMockResponse = new MockResponse(
            self::BODY);

        $client = new MockHttpClient($promoCodeListMockResponse);

        $promoCodeService = new PromoCodeService($client);

        $codeInfo = $promoCodeService->getOffers('NO_OFFERS');

        $this->assertEmpty($codeInfo);
    }

    public function testGasCode(): void
    {
        $promoCodeListMockResponse = new MockResponse(
            self::BODY);

        $client = new MockHttpClient($promoCodeListMockResponse);

        $promoCodeService = new PromoCodeService($client);

        $validOffers = $promoCodeService->getOffers('CODE_GAS');

        $this->assertCount(1, $validOffers);

        $this->assertSame('GAS', $validOffers[0]->getType());
    }

    public function testAllCode(): void
    {
        $promoCodeListMockResponse = new MockResponse(
            self::BODY);

        $client = new MockHttpClient($promoCodeListMockResponse);

        $promoCodeService = new PromoCodeService($client);

        $validOffers = $promoCodeService->getOffers('CODE_ALL');

        $this->assertCount(2, $validOffers);

        $this->assertSame('ELEC_OFFRE_1', $validOffers[1]->getName());
    }
}
